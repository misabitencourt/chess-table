export default `
[
    {
    "pos": "b1",
    "steps": 1
    },
    {
    "pos": "b5",
    "steps": 1
    },
    {
    "pos": "c2",
    "steps": 1
    },
    {
    "pos": "c4",
    "steps": 1
    },
    {
    "pos": "a3",
    "steps": 2
    },
    {
    "pos": "c3",
    "steps": 2
    },
    {
    "pos": "d2",
    "steps": 2
    },
    {
    "pos": "a3",
    "steps": 2
    },
    {
    "pos": "a7",
    "steps": 2
    },
    {
    "pos": "c3",
    "steps": 2
    },
    {
    "pos": "c7",
    "steps": 2
    },
    {
    "pos": "d4",
    "steps": 2
    },
    {
    "pos": "d6",
    "steps": 2
    },
    {
    "pos": "a1",
    "steps": 2
    },
    {
    "pos": "a3",
    "steps": 2
    },
    {
    "pos": "b4",
    "steps": 2
    },
    {
    "pos": "d4",
    "steps": 2
    },
    {
    "pos": "e1",
    "steps": 2
    },
    {
    "pos": "e3",
    "steps": 2
    },
    {
    "pos": "a3",
    "steps": 2
    },
    {
    "pos": "a5",
    "steps": 2
    },
    {
    "pos": "b2",
    "steps": 2
    },
    {
    "pos": "b6",
    "steps": 2
    },
    {
    "pos": "d2",
    "steps": 2
    },
    {
    "pos": "d6",
    "steps": 2
    },
    {
    "pos": "e3",
    "steps": 2
    },
    {
    "pos": "e5",
    "steps": 2
    }
    ]
`;