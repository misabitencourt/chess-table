import React, { PureComponent } from 'react';

import Icon from '../Icon/Icon';

import styles from './ChessTable.css';
class ChessTable extends PureComponent {

  static WIDTH = "12345678";
  static HEIGHT = "abcdefgh";

  getSquareItem(id) {
    if (! this.props.chessTable) {
      return '';
    }

    if (this.props.chessTable.selectedKnightPosition === id) {
      return (<Icon icon='CHESS_KNIGHT' />);
    }
  }

  isHighlight(id) {
    if (! this.props.chessTable) {
      return false;
    }

    if (this.props.chessTable.movesPreview) {
      for (let movePreview of this.props.chessTable.movesPreview)  {
        if (movePreview.pos === id) {
          return true;
        }
      }
    }

    return false;
  }

  render() {
    const {
      selectKnightPosition
    } = this.props;

    return (
      <div className={styles.chessTable}>
        {[...ChessTable.WIDTH].reverse().map(row => {
          return (
            <div key={row}>
              {[...ChessTable.HEIGHT].map(col => {
                const squareId = `${col}${row}`;
                return (
                  <div key={squareId} 
                       className={this.isHighlight(squareId) ? styles.highlighted : ''}
                       onClick={() => selectKnightPosition({position: squareId})}
                       title={squareId}>{this.getSquareItem(squareId)}</div>
                );
              })}
            </div>
          );
        })}
      </div>
    );
  }
}

export default ChessTable;
