import fetchDefaults from '../fetch-defaults';

const memCache = {};

export async function getPiecePreview(piece, position, steps=2) {
    steps = isNaN(steps) ? 2 : steps;
    piece = encodeURIComponent(piece);
    position = encodeURIComponent(position);
    const getUrl = `${fetchDefaults.apiUrl}/moves/preview.json?piece=${piece}&pos=${position}&steps=${steps}`;
    if (memCache[getUrl]) {
        return memCache[getUrl];
    }
    const res = await fetch(getUrl, fetchDefaults.config);
    if (res.status !== 200) {
        const text = await res.text();
        throw {error: text};
    }
    const json = await res.json();
    const moves = json.filter(move => move.steps == steps);
    memCache[getUrl] = moves;

    return moves;
}