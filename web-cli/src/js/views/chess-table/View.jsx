import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'

import LazyLoading from '../../common/components/LazyLoading'
import { actions } from '../../redux/modules/chess-table'

import { importantInfo } from './styles.css';

const ChessTable = LazyLoading(() => import('../../common/components/ChessTable/ChessTable'))

const mapStateToProps = state => state

const mapDispatchToProps = {
  ...actions,
}

@connect(mapStateToProps, mapDispatchToProps)
class ChessTableView extends Component {

  render() {
    return (
      <Fragment>
        <h1>Chess moves preview</h1>
        <ChessTable {...this.props} />
        {this.props.chessTable.selectedKnightPosition ? (
          <p>The green squares represents the possible position(s) in 2 turns.</p>
        ) : (
          <p className={importantInfo}>Select a square to place the knight.</p>
        )}
      </Fragment>
    )
  }
}

export default ChessTableView
