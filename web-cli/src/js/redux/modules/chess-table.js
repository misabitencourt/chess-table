import { createAction, handleActions } from 'redux-actions';
import { getPiecePreview } from '../../common/api/index';
// export const getAwesomeCode = createAction(GET_EXAMPLE, () => ({}));

export const selectKnightPosition = createAction('SELECT_KNIGHT_POS', ({position}) => async (dispatch, getState) => {
  try {
    const preview = await getPiecePreview('knight', position);
    return { selectedKnightPosition: position, movesPreview: preview };
  } catch (e) {
    throw new Error(e);
  }
});

export const actions = {
  selectKnightPosition
  // getAwesomeCode,
  // updateExample,
};

export const reducers = {
  'SELECT_KNIGHT_POS': (state, { payload }) => {
    state.selectedKnightPosition = payload.selectedKnightPosition;
    state.movesPreview = payload.movesPreview;

    return {...state};
  },
}

export const initialState = () => ({selectKnightPosition: null})

export default handleActions(reducers, initialState());
