import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import chessTable from './modules/chess-table';

export default combineReducers({
  chessTable,
  routing,
});
