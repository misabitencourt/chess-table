import React from 'react'
import {
  Route,
  Switch,
} from 'react-router-dom'

import LazyLoading from 'common/components/LazyLoading'

import styles from '../style/index.css'

// This is show case how you can lazy loading component
// const ExampleRouteHandler = LazyLoading(() => import('views/example'))

const ChessTableRoute = LazyLoading(() => import('views/chess-table'))

module.exports = (
  <div className={styles.container}>
    <div className={styles.content}>
      <Switch>
        <Route exact path="/" component={ChessTableRoute} />
        <Route path="*" component={ChessTableRoute} />
      </Switch>
    </div>
  </div>
)
