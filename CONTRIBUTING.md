# Contributing to Chess moves

I apreciate your interest to contribute with this project. You can help with improvements,
bug report, improving the documentation and doing some bugfixes. You are welcome to share
your ideas and critics. Comments or variable names with abusive, harassing or something like that
are unacceptable.

# Project scopes

This project haves two main instances: The backend json API and the web client. The api haves the
intel algorithms and the web cli is a web user interface. The interface has to be accessible to the
major devices like smartphones, tablets and notebooks.

The api backend uses [Rails](https://rubyonrails.org/) framework and follow it principles. The 
web client was made with [React and Redux](https://redux.js.org/basics/usage-with-react). 

![Project Overview](https://bitbucket.org/misabitencourt/chess-table/raw/050a9261038208cf60701fabf4b57c0be667b8b4/docs/overview.png)

# Aplication theme

The web interface look and feel style needs to provide a old gamer nostalgia. For that reason, 
the project style is dark with a font family that looks like a 8bit game. The page styles is 
written with well know CSS and [PostCSS](https://postcss.org/).

# Frontend Development

The frontend javascript is written in ES2017 and the major browsers dont support all the language
features yet (i wrote this in 02/2019). For that reason, (Webpack with Babel)[http://ccoenraets.github.io/es6-tutorial-data/babel-webpack/] are used to transpile
and merge the ES2017 code to a single javascript file with old Javascript code.

If you want to improve or change frontend code, you will need to install the frontend build dependencies. To do that, go to the web-cli dir and run the command:

```
npm install
```

If that depencencies are well installed, you can start a frontend development server, running
on the same directory that command:

```
npm run dev
```

When your frontend development are finished, you can run the build process by that command:

```
npm run build
```

It will generate the frontend compiled files in a new directory called docroot. You can move that
assets to your webserver or the backend public directory.

# Tests

I recomend to make unit tests for the reducers and server endpoints. In the backend, the tests
are written with Rails common libs. In the frontend, it uses [Jest](https://jestjs.io/) to made the tests.

Running tests on backend:
```
./chess-table/bin/rails test
```

Running tests on frontend:
```
cd ./web-cli
npm run test
```


