# Chess Moves

Chess moves is a (Rails + React) web application to show the Knight possible moves on Chess game. It runs
on major modern browsers and works like a charm!

![Chess Moves](https://bitbucket.org/misabitencourt/chess-table/raw/26ae02ef4f64a66e6ffaaed175282249a5b2867c/docs/chess-moves.gif)

# Installing

To install that software in your web server or local machine, you will need a O.S. with [NodeJS](https://nodejs.org/en/) and [Ruby](https://www.ruby-lang.org/en/) installed. 

After clone or download this repository, go to the chess-table directory and run:

```
bundle
```

It will install the Ruby lib dependencies. Now, you can start the web server, running:

```
rails s
```

After run that, you will able to access the software opening your browser and typing the 
server address:

```
http://localhost:3000
```







