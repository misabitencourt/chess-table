require 'test_helper'
require 'json'

class MovesControllerTest < ActionDispatch::IntegrationTest
  test "Moves: validate a single step" do
    get moves_preview_url
    assert_response :bad_request

    get "#{moves_preview_url}?piece=knight"
    assert_response :bad_request

    get "#{moves_preview_url}?piece=dog&pos=a1"
    assert_response :unprocessable_entity

    get "#{moves_preview_url}?piece=knight&pos=a1"
    assert_response :success

    response_data = JSON.parse @response.body
    assert_equal response_data.length, 2
    assert_equal 'b3', response_data[0]['pos']
    assert_equal 'c2', response_data[1]['pos']
  end

  test "Moves: validate two steps moves preview" do
    get "#{moves_preview_url}?piece=knight&pos=a1&steps=2"
    assert_response :success
    response_data = JSON.parse @response.body
    assert_equal response_data.length, 14
    positions = %w(b3 c2 a1 a5 c1 c5 d2 d4 a1 a3 b4 d4 e1 e3)
    response_data.each do |preview|
      assert positions.include? preview['pos']
    end
  end
end
