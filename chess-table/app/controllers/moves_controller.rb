class MovesController < ApplicationController
  def preview
    start_pos = helpers.get_start_pos params[:pos]

    if start_pos.nil?
      return render json: {}, status: :bad_request, layout: false
    end

    steps = params[:steps].blank? ? 1 : Integer(params[:steps])
    moves_preview = helpers.move_preview params[:piece], start_pos, steps
    unless moves_preview[:error].blank?
      return render json: moves_preview, status: :unprocessable_entity, layout: false
    end

    return render json: helpers.pos_to_notation(moves_preview)
  end

end

