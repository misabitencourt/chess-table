module MovesHelper

    COLS_NOTATION_X = "abcdefgh"
    COLS_NOTATION_Y = "12345678"
    CHESS_TABLE_WIDTH = 8
    CHESS_TABLE_HEIGHT = 8

    def move_preview(piece, pos, steps)
        if piece.eql? "knight"
            positions = []
            steps.times do |step|
                if positions.empty?
                    positions = positions.concat preview_knight(piece, [pos], step)
                else
                    positions = positions.concat preview_knight(piece, positions, step)
                end
            end
            return { positions: positions }
        end
        
        {error: "Unknow piece"}
    end

    def get_start_pos(pos_str)
        if pos_str.blank?
            return nil
        end
        
        unless pos_str.length.eql? 2
            return nil
        end

        {x: COLS_NOTATION_X.index(pos_str[0]), y: COLS_NOTATION_Y.index(pos_str[1])}
    end

    def preview_knight(piece, positions, step)
        avaliable_pos = []

        positions.each do |pos|
            unless pos[:step].nil? or (pos.step - 1) == step
                next
            end

            CHESS_TABLE_WIDTH.times do |x|
                CHESS_TABLE_HEIGHT.times do |y|
                    x_diff = Integer(pos[:x]) - x
                    y_diff = Integer(pos[:y]) - y
                    x_diff = (x_diff < 0) ? (x_diff * -1) : x_diff
                    y_diff = (y_diff < 0) ? (y_diff * -1) : y_diff
                    if ((x_diff + y_diff) == 3) and (y_diff > 0) and (x_diff > 0)
                        avaliable_pos << {x: x, y: y, steps: step+1}
                    end
                end
            end
        end

        avaliable_pos
    end

    def pos_to_notation(moves)
        positions = moves[:positions]
        converted = []
        positions.each do |pos|
            x = pos[:x]
            y = pos[:y]
            pos_str = "#{COLS_NOTATION_X[pos[:x]]}#{COLS_NOTATION_Y[pos[:y]]}"
            converted << {pos: pos_str, steps: pos[:steps]}
        end

        converted
    end
end
